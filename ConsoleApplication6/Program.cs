﻿using System;
using System.Diagnostics;

namespace ConsoleApplication6
{
    class Program
    {

        private class RunnerService
        {
            public void Run(Action runnable) => runnable();
        }

        static void Main(string[] args)
        {

            var stopWatch = new Stopwatch();
            var runService = new RunnerService();

            stopWatch.Start();
            for (var i = 0; i < 10000; i++)
                WriteSomething($"Iteration method nº {i}, {DateTime.Now:T}");
            stopWatch.Stop();
            var actionsTimer = stopWatch.Elapsed;

            stopWatch.Restart();
            for (var j = 0; j < 10000; j++)
                runService.Run(() => WriteSomething($"Iteration action nº {j}, {DateTime.Now:T}"));
            stopWatch.Stop();
            var methodsTimer = stopWatch.Elapsed;

            Console.WriteLine($"Iterating actions: {actionsTimer:T}");
            Console.WriteLine($"Iterating method: {methodsTimer:T}");
            Console.ReadLine();

        }

        public static void WriteSomething(string something) => Console.WriteLine(something);
    }
}
